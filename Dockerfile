FROM tensorflow/tensorflow:1.14.0-py3-jupyter

RUN apt-get install -y x11-apps
RUN pip install opencv-python tqdm scikit-video pyrealsense2 && apt install -y libxext6 libxrender-dev libsm6

# install libraries
RUN apt-get update \
    && apt-get install --no-install-recommends -q -y \
    libglfw3-dev \
    libgtk-3-dev \
    libssl-dev \
    libusb-1.0-0-dev git \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
    

# setup keys for intel's repository server
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key C8B3A55A6F3EFCDE
RUN echo "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo $(lsb_release -cs) main" \
    > /etc/apt/sources.list.d/realsense-public.list

RUN echo "export QT_X11_NO_MITSHM=1" >> /etc/bash.bashrc

#RUN git clone https://github.com/IntelRealSense/librealsense.git && cd librealsense
