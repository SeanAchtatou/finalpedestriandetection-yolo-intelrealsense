import cv2
from pathlib import Path


distance = str(input("Creation distance >>"))


video = cv2.VideoWriter(f'video_out{distance}m.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (640,480))

path = Path(f'./data/Data_RealCameraSense_Recording/Output_YOLO_Process/Output_{distance}m')

imgColor = list(path.glob(f'*ResultFrame{distance}m.jpg'))

listPictures = []
listColor = []

print("Creation of the video...")

for i in range(len(imgColor)):
    listColor.append(imgColor[i])

listColor = sorted(listColor)
  
for i in range(len(listColor)):
    listPictures.append(listColor[i])


for k in listPictures: 
    img_ori = cv2.imread(f'./{k}')
    video.write(img_ori)

print("Done.")
video.release()