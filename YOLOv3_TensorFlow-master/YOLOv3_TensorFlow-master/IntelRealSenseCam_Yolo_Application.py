# coding: utf-8

from __future__ import division, print_function

import tensorflow as tf
import numpy as np
import argparse
import cv2
import time
import pyrealsense2 as rs

from utils.misc_utils import parse_anchors, read_class_names
from utils.nms_utils import gpu_nms
from utils.plot_utils import get_color_table, plot_one_box
from utils.data_aug import letterbox_resize

from model import yolov3

parser = argparse.ArgumentParser(description="YOLO-V3 video test procedure.")
parser.add_argument("input_video", type=str,
                    help="The path of the input video.")
parser.add_argument("--anchor_path", type=str, default="./data/yolo_anchors.txt",
                    help="The path of the anchor txt file.")
parser.add_argument("--new_size", nargs='*', type=int, default=[416, 416],
                    help="Resize the input image with `new_size`, size format: [width, height]")
parser.add_argument("--letterbox_resize", type=lambda x: (str(x).lower() == 'true'), default=True,
                    help="Whether to use the letterbox resize.")
parser.add_argument("--class_name_path", type=str, default="./data/coco.names",
                    help="The path of the class names.")
parser.add_argument("--restore_path", type=str, default="./data/darknet_weights/yolov3.ckpt",
                    help="The path of the weights to restore.")
parser.add_argument("--save_video", type=lambda x: (str(x).lower() == 'true'), default=False,
                    help="Whether to save the video detection results.")
args = parser.parse_args()

args.anchors = parse_anchors(args.anchor_path)
args.classes = read_class_names(args.class_name_path)
args.num_class = len(args.classes)

color_table = get_color_table(args.num_class)

with tf.Session() as sess:
    input_data = tf.placeholder(tf.float32, [1, args.new_size[1], args.new_size[0], 3], name='input_data')
    yolo_model = yolov3(args.num_class, args.anchors)
    with tf.variable_scope('yolov3'):
        pred_feature_maps = yolo_model.forward(input_data, False)
    pred_boxes, pred_confs, pred_probs = yolo_model.predict(pred_feature_maps)

    pred_scores = pred_confs * pred_probs

    boxes, scores, labels = gpu_nms(pred_boxes, pred_scores, args.num_class, max_boxes=200, score_thresh=0.3, nms_thresh=0.45)

    saver = tf.train.Saver()
    saver.restore(sess, args.restore_path)

    class VideoFrameGenerator():
        def __init__(self):
            self.pipeline = rs.pipeline()
            self.cfg = rs.config()
            self.cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
            self.cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
            self.cameraRS = self.pipeline.start(self.cfg)
            
            self.outVideo = cv2.VideoWriter('CameraRealSenseLIVE.avi', cv2.VideoWriter_fourcc(*'MPEG'), 20, (640,480)) 
                
        def gen(self):
            while True:

                frame = self.pipeline.wait_for_frames()   #wait for frame from cameraRS
                color_frame = frame.get_color_frame()     #get the color frame 
                depth_frame = frame.get_depth_frame()     #get the depth(laser)frame

                frameColor = np.asanyarray(color_frame.get_data())     #represent the color frame as array

                colorizer = rs.colorizer()
                frameDepth = np.asanyarray(colorizer.colorize(depth_frame).get_data())  #colorize the depth frame 

                align = rs.align(rs.stream.color) #just to align both frame together (because of noise)
                frameset = align.process(frame)
                aligned_depth_frame = frameset.get_depth_frame()
                colorAlignDepthFrame = np.asanyarray(colorizer.colorize(aligned_depth_frame).get_data())
                bothFrame = np.hstack((frameColor, colorAlignDepthFrame))
                finalDepthFrame = np.asanyarray(aligned_depth_frame.get_data())

                img_ori = np.asanyarray(color_frame.get_data()) #get the color frame as array
  
                if args.letterbox_resize:
                    img, resize_ratio, dw, dh = letterbox_resize(img_ori, args.new_size[0], args.new_size[1])
                else:
                    height_ori, width_ori = img_ori.shape[:2]
                    img = cv2.resize(img_ori, tuple(args.new_size))
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img = np.asarray(img, np.float32)
                img = img[np.newaxis, :] / 255.
 
                start_time = time.time()
                boxes_, scores_, labels_ = sess.run([boxes, scores, labels], feed_dict={input_data: img})
                end_time = time.time()
                
                # rescale the coordinates to the original image
                if args.letterbox_resize:
                    boxes_[:, [0, 2]] = (boxes_[:, [0, 2]] - dw) / resize_ratio
                    boxes_[:, [1, 3]] = (boxes_[:, [1, 3]] - dh) / resize_ratio
                else:
                    boxes_[:, [0, 2]] *= (width_ori/float(args.new_size[0]))
                    boxes_[:, [1, 3]] *= (height_ori/float(args.new_size[1]))
                   
                print("Number of boxes:",len(labels_), labels_)
                for i in range(len(boxes_)):
                    x0, y0, x1, y1 = boxes_[i]                 
                    if args.classes[labels_[i]] == "person":   
                        print("Status: Someone has been detected !") 
                        print("    Class per boxe:",args.classes[labels_[i]]) 
                        plot_one_box(img_ori, [x0, y0, x1, y1], label=args.classes[labels_[i]] + ', {:.2f}%'.format(scores_[i] * 100), color=color_table[labels_[i]])
                cv2.putText(img_ori, '{:.2f}ms'.format((end_time - start_time) * 1000), (40, 40), 0,
                            fontScale=1, color=(0, 255, 0), thickness=2)
            
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

                if depth_frame:
                    self.outVideo.write(img_ori)
                    count = -1
                    for i in boxes_:
                        count += 1
                        if args.classes[labels_[count]] == "person":
                            print("Boxe:", i)
                            crop_image = finalDepthFrame[int(i[1]):int(i[3]),int(i[0]):int(i[2])]
                            depth_scale = self.cameraRS.get_device().first_depth_sensor().get_depth_scale()
                            print(f'DEPTH SCALE: {depth_scale}')
                            depthCal = crop_image * depth_scale
                            try:                       
                                pixelMin = np.quantile(depthCal,0.25)
                                dist,_,_,_ = cv2.mean(depthCal[depthCal <= pixelMin])
                                print("Distance:",dist)
                                cv2.putText(img_ori,'Distance: ' + str('{0:.5f}'.format(dist)), (i[0], i[3]), 0,
                                fontScale=0.40, color=(0, 0, 255), thickness=1, lineType=cv2.LINE_AA)
                            
                            except:
                                print("Error quantile.")
                                        
                yield img_ori
                        
            self.cap.release()
            self.outVideo.release()
            cv2.destroyAllWindows()
            self.pipeline.stop()
                             
    getVideo = VideoFrameGenerator()             #get the video

    for i in getVideo.gen():
        cv2.imshow("CameraFrame", i)
        
             
                
