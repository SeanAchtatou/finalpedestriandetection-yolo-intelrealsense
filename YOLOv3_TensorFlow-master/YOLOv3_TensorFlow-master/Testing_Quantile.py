
import cv2
import numpy as np


quantile = float(input("Percentage of quantile to be used (between 0.1 and 0.9):"))
a = np.random.randint(1,10,10)
print(f'Values : {a}')
print(f"Doing a quantile on the values with percentage {quantile}")

b = np.quantile(a,quantile)
print(f'Average of the values below the quantile percentage : {b}')
print(f'Values below the quantile percentage in boolean : {a <= b}')
print(f'Final values obtained : {a[a <= b]}')
print(f'Distance : {np.mean(a[a<=b])}')




