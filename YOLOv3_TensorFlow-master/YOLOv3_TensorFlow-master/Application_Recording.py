import pyrealsense2 as rs
import numpy as np
import cv2

distance = str(input("Recording distance >>"))

class VideoFrameGenerator():

    def __init__(self):
        self.count = 0
        self.pipeline = rs.pipeline()
        self.cfg = rs.config()
        self.cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        self.cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
        self.cameraRS = self.pipeline.start(self.cfg)
            
    def gen(self):
        while True:
            self.count += 1
            frame = self.pipeline.wait_for_frames()
            depth_frame = frame.get_depth_frame()
            color_frame = frame.get_color_frame()
            frameColor = np.asanyarray(color_frame.get_data())     #represent the color frame as array

            colorizer = rs.colorizer()
            frameDepth = np.asanyarray(colorizer.colorize(depth_frame).get_data())  #colorize the depth frame 

            align = rs.align(rs.stream.color) #just to align both frame together (because of noise)
            frameset = align.process(frame)
            aligned_depth_frame = frameset.get_depth_frame()
            colorAlignDepthFrame = np.asanyarray(colorizer.colorize(aligned_depth_frame).get_data())
            bothFrame = np.hstack((frameColor, colorAlignDepthFrame))

            DepthFrameAligned = np.asanyarray(aligned_depth_frame.get_data())
      
            depth_scale = self.cameraRS.get_device().first_depth_sensor().get_depth_scale()
            finalDepthFrame = DepthFrameAligned * depth_scale
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            cv2.imwrite(f'./data/Data_RealCameraSense_Recording/Input_Frame_Folder/Frame_{distance}m/{self.count:05d}ColorFrame{distance}m.jpg',frameColor)
            cv2.imwrite(f'./data/Data_RealCameraSense_Recording/Input_Frame_Folder/Frame_{distance}m/{self.count:05d}DepthFrame{distance}m.jpg',finalDepthFrame)
            
            yield frameColor
                        

getVideo = VideoFrameGenerator()             #get the video

for i in getVideo.gen():
    cv2.imshow("CameraFrame", i)
      
             