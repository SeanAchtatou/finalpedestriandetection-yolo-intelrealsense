import cv2
import time


class VideoFrameGenerator():
	def __init__(self, path):
         self.cap = cv2.VideoCapture(0)
         print(self.cap.get(5))
                   
	def gen(self):
		while self.cap.isOpened():
			successFrame, frame = self.cap.read()
            
			if successFrame:
				yield frame # None
                
	def release(self):
		self.cap.release()


genVideo = VideoFrameGenerator("out.mp4")          # get __init__ from the class VideoFrameGenerator
for frame in genVideo.gen():
	#t = time.time()
    cv2.imshow("Frame", frame)
    
	#frame, depth = process(frame)        
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
	#else:
		#time.sleep(max(0, 0.03 - (time.time()-t)))
	
genVideo.release()
cv2.destroyAllWindows()