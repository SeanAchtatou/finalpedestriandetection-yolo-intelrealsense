#!/bin/bash
xhost +
docker run --rm -it \
        -p 8888:8888 \
        --privileged \
        --net=host \
        -e DISPLAY=$DISPLAY \
        -v /dev:/dev \
        -v $(pwd):/tf sean/tensorflow bash